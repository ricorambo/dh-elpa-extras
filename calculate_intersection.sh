#!/bin/sh

# Calculates the intersection between the packages maintained by the team and
# the packages already provided by emacs.

# The intersection is written to stdout, in "elpa-*" form

if test $# -lt 2; then
    echo "Missing arguments"
    echo "Usage: $0 <debian-emacsen file> <emacs file>"
    exit 22
fi

temp_debian_emacsen=$(mktemp)
cut -d' ' -f 1 $1 | sort > $temp_debian_emacsen

temp_emacs=$(mktemp)
cut -d' ' -f 1 $2 | sed -E "s/^/elpa-/g" | sort > $temp_emacs

comm -12 $temp_debian_emacsen $temp_emacs

rm -f $temp_debian_emacsen $temp_emacs

