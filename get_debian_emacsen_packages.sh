#!/bin/sh

# Outputs the list of packages maintained by the team, minus transition packages

unstable_chroot=chroot:unstable-amd64-sbuild

debian_emacsen_packages=$(mktemp)
debian_emacsen_packages2=$(mktemp)

schroot -c $unstable_chroot -u root -s /bin/bash -d /root -- \
	bash -c 'apt update 1>/dev/null 2>/dev/null && \
apt install -y aptitude 1>/dev/null 2>/dev/null && \
aptitude search "?maintainer(debian-emacsen@lists.debian.org)?or(?architecture(all),?architecture(any))" 2>/dev/null' 1> $debian_emacsen_packages

# We get rid of the transition packages
grep -E -i -v "transition|dummy" $debian_emacsen_packages > $debian_emacsen_packages2

tr -s ' ' < $debian_emacsen_packages2 | cut -d' ' -f 2

rm -f $debian_emacsen_packages $debian_emacsen_packages2

