#!/bin/sh

# Reads package names from stdin, in "elpa-*" form, gets the version emacs
# provides them for from the file passed as only argument, and outputs the
# correct "Provides" and "Breaks" on stdout.

if test $# -lt 1; then
    echo "Missing argument"
    echo "Usage : $0 <File containing emacs packages and versions>"
    exit 22
fi

# Fix with correct value
unstable_chroot=chroot:unstable-amd64-sbuild

tempfile=$(mktemp)

provides="Provides:"
breaks="Breaks:"

while read line; do
    stub=$(echo $line | sed -E "s/^elpa-//g")
    version=$(grep "^$stub " $1 | cut -d' ' -f 2)

    if test $version = "nil"; then
	# package--builtins incorrectly reports a nil version, let us correct it
	# ourselves using the header in the elisp file directly

	# Let us install emacs-el in a chroot and try and find the real version

	schroot -c $unstable_chroot -u root -s /bin/bash -d root -- > $tempfile <<EOF
apt update 1>/dev/null 2>/dev/null
apt -y install emacs-el 1>/dev/null 2>/dev/null
library_file=\$(find /usr/share/emacs/ -regextype "posix-extended" -regex ".*/$stub\.el\.(.*)?")

if test -z \$library_file; then
   exit
fi

extension=\$(echo \$library_file | sed "s/^.*\.//")

if test \$extension = "el"; then
    grep ";; Package-Version: " \$library_file | sed "s/;; Package-Version: //g"
elif test \$extension = "gz"; then
    zgrep ";; Package-Version: " \$library_file | sed "s/;; Package-Version: //g"
fi

EOF

	# Let us verify that the temporary file contains something correct
	if test ! $(cat $tempfile) = $(cat $tempfile | egrep "^[[:digit:]](\.[[:digit:]])*$"); then
	    continue
	fi

	# Let us finally accept this as version
	version=$(cat $tempfile)

    fi

    provides="$provides $line (= $version),"
    breaks="$breaks $line (<= $version),"

done

echo $provides | sed "s/, /,\n /g"
echo $breaks | sed "s/, /,\n /g"

rm $tempfile

