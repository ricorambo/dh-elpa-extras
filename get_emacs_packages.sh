#!/bin/sh

# Outputs the list of packages provided by the version of emacs currently in
# unstable

# Fix with correct value
unstable_chroot=chroot:unstable-amd64-sbuild

emacs_packages=$(mktemp)

schroot -c $unstable_chroot -u root -s /bin/bash -d /root -- \
	bash -c "apt update 1>/dev/null 2>/dev/null && \
apt install -y emacs 1>/dev/null 2>/dev/null && \
emacs --batch \
--eval \"(progn (require (quote finder-inf)) (print package--builtins))\" \
2> /dev/null" 1> $emacs_packages

sed -i "/^$/d" $emacs_packages
sed -i "s/) (/)\n(/g" $emacs_packages
sed -E -i "s/^\(+//g" $emacs_packages
sed -E -i "s/\)+$//g" $emacs_packages
sed -E -i "s/\[(nil|\([^\)]*\)).*\]/\1/g" $emacs_packages
sed -E -i "s/([-[:digit:]]) ([-[:digit:]])/\1.\2/g" $emacs_packages
sed -E -i "s/([-[:digit:]]) ([-[:digit:]])/\1.\2/g" $emacs_packages
sed -E -i "s/\. //g" $emacs_packages
tr -d '()' < $emacs_packages

rm -f $emacs_packages

